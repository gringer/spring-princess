extends CharacterBody2D

var holdingJump = false
var movingLeft = false
var movingRight = false
var jumping = false
var rewindMode = false
var onSnow = false
var onIce = true
var sliding = false
var rewindUsed = false
var registeredFall = true
var fallCount = 0
var jumpFrames: int = 0
var lastJumpFrames = 0
var jumpStartYPos = 0
var jumpFromName = ""
var jumpCompletionName = ""
var jumpFromFrameMin = 0
var jumpFromFrameMax = 0
var gravity = 980
var moveVelocity = Vector2(0, 0)
var screenYCount = -1
var standingCooldown = 0
var lastZeroY = 0
var lastScreenY = -1
var rewindPos: Array
var rewindVel: Array
var rewindCycle: Array

## TODO: area ideas
### 1. Tree base (based around protected Kauri forest)
### 2. Tree canopy (artificially lit)
### 3. Mining area
### 4. Slums
### 5. Upper-class housing
### 6. Clouds
### 7. Library (ref: Tunic library in the clouds)
### 8. Crystal palace
### 9. Tower / parapet

## TODO: script for top
### [Scene: Frog Prince is at the top of the tower, springing up and down]
### Frog prince: "Hey! Over here! Kiss me, please!"
### [Kiss / transformation scene]
### If rewind not used, and fall count > 10: "Oh, did you know you can rewind time with <Shift>?"
### If rewind used: "You used rewind? Pfft, cheater - anyone could do that!"
### If rewind not used, fall 1-9: "You hardly fell at all of the way up here? Must be an easy course."
### If rewind not used, 0 falls: "No falls at all? Must be so easy that I could do blindfolded!"
### [All scenarios end with the Spring Princess getting angry and 
###  pushing the Frog Prince off the top of the tower]

const screenHeight = 360
const screenWidth = 480
const xMinMoveLength = 205
const xMaxMoveLength = 205
const maxJumpMul = 16.1
const slideSpeed = 1
const maxFrameCount = 35
const bounceDampFracY = 1
const bounceDampFracX = 0.55
const frameTime = 1.0 / 60
const minVelocityX = 2
const maxWindSpeed = 6

@onready var rootNode: Node2D = get_parent()
@onready var background: TextureRect = rootNode.get_node("JumpkingMapReduced")
@onready var camera: Camera2D = rootNode.get_node("Camera2D")
@onready var fallLabel: Label = camera.get_node("fallLabel")
@onready var jumpProgress: ProgressBar = camera.get_node("jumpProgress")
@onready var animation: AnimatedSprite2D = $AnimatedSprite2D
@onready var standingCollision: CollisionPolygon2D = $Collision_Standing
@onready var fallingCollision: CollisionPolygon2D = $Collision_Falling
@onready var debugLabel: Label = $Label
@onready var note110: AudioStreamPlayer = rootNode.get_node("Note_110")
@onready var note220: AudioStreamPlayer = rootNode.get_node("Note_220")
@onready var note440: AudioStreamPlayer = rootNode.get_node("Note_440")
@onready var note880: AudioStreamPlayer = rootNode.get_node("Note_880")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	if(holdingJump):
		if(movingLeft):
			animation.frame = 1
		elif(movingRight):
			animation.frame = 3
		else:
			animation.frame = 2
	elif(jumping and (standingCooldown == 0)):
		animation.frame = 4
	else:
		animation.frame = 0
	pass

func activateJump(jumpName: String, frameMin: int, frameMax: int):
	jumpFromName = jumpName
	jumpFromFrameMin = frameMin
	jumpFromFrameMax = frameMax
	print("Pre-activated jump %s with %d-%d frames" % [jumpFromName, jumpFromFrameMin, jumpFromFrameMax])

func deactivateJump(jumpName: String):
	if(jumpFromName != ""):
		if(jumpFromName == jumpName):
			jumpCompletionName = jumpFromName
			print("Jump %s will complete (if it lands)" % jumpFromName)
		else:
			print("Jump %s doesn't match jump %s" % [jumpFromName, jumpCompletionName])
	else:
		print("No from jump exists")

func leaveJump(_jumpName: String):
	pass

# Called at a constant rate, not linked to displayed frames
func _physics_process(delta):
	## Use 7 notes per octave
	## B A G F E D C 
	## https://www.liutaiomottola.com/formulae/freqtab.htm
	const penTone = [493.883 / 440, 1.0, 391.995 / 440, 349.228 / 440,
					 329.628 / 440, 293.665 / 440, 261.626 / 440]
	var windFrame = Time.get_ticks_msec() % 13000
	var windMul = sin(windFrame / 13000.0 * 2 * PI)
	if(abs(windMul) > 0.75):
		windMul = 0.75 * sign(windMul)
	windMul = windMul / 0.75
	var frameCount = delta / frameTime
	screenYCount = -(camera.position.y + screenHeight / 2.0) / screenHeight + 2
	var moveAccel = Vector2(0,0)
	rewindMode = Input.is_action_pressed("rewind")
	if !rewindMode:
		movingLeft = Input.is_action_pressed("move_left")
		movingRight = Input.is_action_pressed("move_right")
		if(!jumping):
			if Input.is_action_pressed("jump"):
				if(!holdingJump):
					jumpFrames = 0
				holdingJump = true
			else:
				holdingJump = false
			if ((!holdingJump and (jumpFrames > 0)) or (jumpFrames >= maxFrameCount)):
				if(jumpFrames < 1):
					jumpFrames = 1
				if(jumpFrames < 36):
					var relNote = jumpFrames % 7
					var relOctave = jumpFrames / 7
					if(relOctave < 1):
						note880.pitch_scale = penTone[relNote] * pow(2, -relOctave + 1)
						note880.play()
					elif(relOctave < 2):
						note440.pitch_scale = penTone[relNote] * pow(2, -relOctave + 2)
						note440.play()
					elif(relOctave < 3):
						note220.pitch_scale = penTone[relNote] * pow(2, -relOctave + 3)
						note220.play()
					else:
						note110.pitch_scale = penTone[relNote] * pow(2, -relOctave + 4)
						note110.play()
				jumpProgress.value = jumpFrames
				moveVelocity.y = -jumpFrames * maxJumpMul
				lastJumpFrames = jumpFrames
				print("Jumped with %d frames power" % lastJumpFrames)
				if(jumpFromName != ""):
					if((jumpFrames >= jumpFromFrameMin) && (jumpFrames <= jumpFromFrameMax)):
						print("  Correctly leaving jump %s" % jumpFromName)
					else:
						print("  Deactivating jump %s (frames don't match)" % jumpFromName)
						jumpFromName = ""
						jumpCompletionName = ""
						jumpFromFrameMin = 0
						jumpFromFrameMax = 0
				jumpFrames = 0
				holdingJump = false
				jumping = true
				standingCooldown = 0
				jumpStartYPos = position.y
				registeredFall = false
				if movingLeft:
					moveAccel.x = -xMaxMoveLength
				elif movingRight:
					moveAccel.x = xMaxMoveLength
			elif(holdingJump and !jumping):
				jumpFrames += frameCount
				jumpProgress.value = jumpFrames
			elif(!holdingJump):
				# short jumps can go slightly further than long jumps; scale appropriately
				var moveScale = ((maxFrameCount - jumpFrames) / maxFrameCount) * \
					(xMaxMoveLength - xMinMoveLength) + xMinMoveLength
				if (!onSnow and movingLeft):
					moveAccel.x -= moveScale/6
					if(moveAccel.x < -moveScale/2):
						moveAccel.x = -moveScale/2
				elif (!onSnow and movingRight):
					moveAccel.x += moveScale/6
					if(moveAccel.x > moveScale/2):
						moveAccel.x = moveScale/2
				else:
					if(onSnow or (abs(moveVelocity.x) < minVelocityX)):
						moveVelocity.x = 0
					else:
						if(onIce):
							moveVelocity.x = moveVelocity.x * 0.98
						else:
							moveVelocity.x = moveVelocity.x * 0.8
		if((screenYCount >= 25) and (screenYCount <= 31)):
			if((screenYCount == 25) || (screenYCount == 31)):
				moveAccel.x += windMul * maxWindSpeed / 3
			else:
				moveAccel.x += windMul * maxWindSpeed
		moveVelocity += moveAccel
		if(moveVelocity != Vector2(0,0)):
			rewindPos.push_back(position)
			rewindVel.push_back(moveVelocity)
	elif(rewindPos.size() > 0):
		rewindUsed = true
		jumpFromName = ""
		jumpCompletionName = ""
		if(!registeredFall):
			fallCount += 1
			registeredFall = true
			fallLabel.text = "Falls%s: %s" % [" / rewinds", fallCount]
		position = rewindPos.pop_back()
		moveVelocity = rewindVel.pop_back()
	moveVelocity.y += gravity * delta
	if(!jumping and (abs(moveVelocity.x) > (xMaxMoveLength))):
		moveVelocity.x = sign(moveVelocity.x) * (xMaxMoveLength)
	if(moveVelocity.y > 800):
		moveVelocity.y = 800
	velocity = moveVelocity
	var cameraPosY = (camera.position.y + screenHeight / 2.0) - position.y
	while(cameraPosY >= screenHeight):
		camera.position.y -= screenHeight
		cameraPosY = (camera.position.y + screenHeight / 2.0) - position.y
	while(cameraPosY < 0):
		camera.position.y += screenHeight
		cameraPosY = (camera.position.y + screenHeight / 2.0) - position.y
	if(!registeredFall and (sliding or (position.y > (lastZeroY + 100)))):
		jumpFromName = ""
		jumpCompletionName = ""
		fallCount += 1
		registeredFall = true
		if(rewindUsed):
			fallLabel.text = "Falls%s: %s" % [" / rewinds", fallCount]
		else:
			fallLabel.text = "Falls: %s" % fallCount
	if(holdingJump and !onIce):
		velocity.x = 0
	if(jumping and (standingCooldown == 0)):
		standingCollision.disabled = true
		fallingCollision.disabled = false
	elif(jumping):
		standingCooldown -= 1
	else:
		standingCollision.disabled = false
		fallingCollision.disabled = true
	var collision = move_and_collide(velocity * delta)
	if collision:
		sliding = false
		var collisionShape: Node2D = collision.get_collider_shape()
		if(collision.get_normal() == Vector2(0, -1)): # on floor
			if(collisionShape.has_meta("surface") and \
				(collisionShape.get_meta("surface") == "snow")):
				debugLabel.text = "snow"
				onSnow = true
			elif(collisionShape.has_meta("surface") and \
				(collisionShape.get_meta("surface") == "ice")):
				# TODO: fix ice slide -> horizontal platform transition
				debugLabel.text = "ice"
				onIce = true
			else:
				onSnow = false
				onIce = false
			lastZeroY = position.y
			jumping = false
			standingCooldown = 20
			if(jumpCompletionName != ""):
				print("Completed jump %s" % jumpCompletionName)
				rootNode.get_node(jumpCompletionName).queue_free()
				jumpCompletionName = ""
			lastJumpFrames = 0
			velocity = velocity.slide(collision.get_normal())
		elif(abs(collision.get_angle() * (180/PI) - 45) < 5):
			jumping = true
			sliding = true
			position += collision.get_depth() * collision.get_normal()
			velocity = velocity.slide(collision.get_normal())
			velocity *= slideSpeed
		elif(collisionShape.has_meta("surface") and \
			(collisionShape.get_meta("surface") == "topWall")):
			velocity = velocity.slide(collision.get_normal())
			velocity.x = 0
			velocity.y *= bounceDampFracY
		else:
			#debugLabel.text = str(collision.get_normal())
			velocity = velocity.bounce(collision.get_normal())
			velocity.x *= bounceDampFracX
			velocity.y *= bounceDampFracY
	if(!jumping && ((position.y > lastZeroY) || (velocity.y > 200))):
		jumping = true
		standingCooldown = 5
	moveVelocity = velocity
	rotation_degrees = 0
