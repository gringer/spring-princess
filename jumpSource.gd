extends Area2D


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _on_body_shape_entered(body_rid, body, body_shape_index, local_shape_index):
	if(body.name == "Princess_CharacterBody2D"):
		if(local_shape_index == 0):
			body.activateJump(name, get_meta("FrameMin"), get_meta("FrameMax"))
		else:
			print("Entered second area for jump %s" % name)
			body.deactivateJump(name)


func _on_body_shape_exited(_body_rid, body, _body_shape_index, local_shape_index):
	if(body.name == "Princess_CharacterBody2D"):
		if(local_shape_index == 0):
			body.leaveJump(name)
