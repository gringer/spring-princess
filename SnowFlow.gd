extends CPUParticles2D

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called at a constant rate, not linked to displayed frames
func _physics_process(_delta):
	var timeFrame = Time.get_ticks_msec() % 13000
	#gravity.x = -200
	gravity.x = sin(timeFrame / 13000.0 * 2 * PI) * 400
	pass
